import { Helmet } from 'react-helmet-async';
import Layout from '../components/Layout/Layout';
import { useFetchWords } from '../api/apiHook';
import Game from '../components/Game/Game';

const Homepage = (): JSX.Element => {
  const { words, error, isLoaded } = useFetchWords(
    'https://technical-exercice-stack-labs.s3.eu-west-3.amazonaws.com/hangman/technos/list',
  );

  if (error !== null) {
    return <div>Error: {error && error.message}</div>;
  }

  if (!isLoaded) {
    return <div>Loading...</div>;
  }
  return (
    <Layout>
      <div className="main__body">
        <div className="container">
          <h1>Hangman&apos;s game</h1>
          <hr />
          <h2>Try to guess the word before the man is hanged !</h2>
          <Game words={words} />
        </div>
      </div>
      <Helmet>
        <title>Hangman&apos;s game</title>
      </Helmet>
    </Layout>
  );
};
export default Homepage;
