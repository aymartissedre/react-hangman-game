import './assets/scss/main.scss';
import { FC } from 'react';
import Homepage from './pages/Homepage';
import ErrorBoundary from './components/ErrorBoundary/ErrorBoundary';

const App: FC = () => (
  <div className="App">
    <ErrorBoundary>
      <Homepage />
    </ErrorBoundary>
  </div>
);

export default App;
