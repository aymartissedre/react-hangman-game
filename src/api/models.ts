export interface WordType {
  word: string;
  image?: string;
}
