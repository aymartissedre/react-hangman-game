import axios from 'axios';
import { useEffect, useState } from 'react';
import { WordType } from './models';

export const useFetchWords = (
  url: string,
): { error: Error | null; isLoaded: boolean; words: WordType[] } => {
  const [words, setWords] = useState<WordType[]>([]);
  const [isLoaded, setIsLoaded] = useState<boolean>(false);
  const [error, setError] = useState<Error | null>(null);

  useEffect(() => {
    const fetchData = () => {
      axios
        .get(url)
        .then((response) => {
          setWords(response.data);
          setIsLoaded(true);
        })
        // eslint-disable-next-line @typescript-eslint/no-shadow
        .catch((error) => {
          setError(error);
        });
    };
    fetchData();
  }, [url]);

  return { error, isLoaded, words };
};

export default useFetchWords;
