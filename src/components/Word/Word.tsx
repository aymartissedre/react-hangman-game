import React, { FC } from 'react';
import './_word.scss';
import { WordType } from '../../api/models';

interface WordProps {
  word: WordType;
  correctKeys: string[];
}

/**
 * Returns each letter of the word, or an underscore if they're not in the correct keys
 * @param word to guess
 * @param correctKeys
 * @param isGameWon
 * @constructor
 */
const Word: FC<WordProps> = ({ word, correctKeys }) => {
  function renderLetter(letter: string) {
    if (letter === ' ') {
      return ' ';
    }
    return correctKeys.includes(letter) ? letter : '_';
  }

  function renderWord() {
    return word.word.split('').map((letter) => renderLetter(letter));
  }

  return (
    <div className="word">
      <span className="word__letters">{renderWord()}</span>
    </div>
  );
};

export default Word;
