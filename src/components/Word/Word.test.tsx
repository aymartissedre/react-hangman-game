import { render } from '@testing-library/react';
import Word from './Word';
import { WordType } from '../../api/models';

describe('Word component', () => {
  it('should display only the correct letters input by the user', () => {
    const testWord: WordType = {
      word: 'Testing',
      image: 'https://www.example.com/random-img.jpg',
    };
    const correctLetters = ['e', 's', 'i', 'n'];
    const { container } = render(
      <Word word={testWord} correctKeys={correctLetters} />,
    );
    const word = container.querySelector('.word__letters')?.innerHTML;
    correctLetters.forEach((letter) =>
      expect(word?.includes(letter)).toBeTruthy(),
    );
    // there should be 3 underscores left (unguessed letters)
    // @ts-ignore
    expect([...word].filter((letter) => letter === '_').length).toBe(3);
  });
});
