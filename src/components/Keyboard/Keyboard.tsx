import KeyboardReact from 'react-simple-keyboard';
import { FC } from 'react';
import 'react-simple-keyboard/build/css/index.css';
import './_keyboard.scss';

interface KeyboardReactProps {
    /* eslint-disable */
  onKeyPress?: (button: string) => void;
  pressedKeys: { correctKeys: string; incorrectKeys: string };
}

const Keyboard: FC<KeyboardReactProps> = ({
  onKeyPress,
  pressedKeys = { incorrectKeys: 'a', correctKeys: 'q' },
}) => (
  <KeyboardReact
    layout={{
      default: ['a z e r t y u i o p', 'q s d f g h j k l m', 'w x c v b n'],
    }}
    onKeyPress={onKeyPress}
    buttonTheme={[
      {
        class: 'hg-red',
        buttons: pressedKeys.incorrectKeys,
      },
      {
        class: 'hg-green',
        buttons: pressedKeys.correctKeys,
      },
    ]}
  />
);

export default Keyboard;
