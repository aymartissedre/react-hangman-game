import { render } from '@testing-library/react';
import Picture from './Picture';

describe('Picture component', () => {
  it('should render as many SVG parts as there are incorrect keys', () => {
    const incorrectKeys = ['a', 'b', 'c', 'd'];
    const { container, rerender } = render(
      <Picture incorrectKeys={incorrectKeys} />,
    );
    expect(container.querySelectorAll('.picture__hangman > *').length).toBe(
      incorrectKeys.length,
    );
    incorrectKeys.push('e');
    rerender(<Picture incorrectKeys={incorrectKeys} />);
    expect(container.querySelectorAll('.picture__hangman > *').length).toBe(
      incorrectKeys.length,
    );
  });

  it('should always render the scaffold', () => {
    const incorrectKeys = [''];
    const { container, rerender } = render(
      <Picture incorrectKeys={incorrectKeys} />,
    );
    expect(container.querySelector('.picture__scaffold')).toBeInTheDocument();
    incorrectKeys.push('e');
    rerender(<Picture incorrectKeys={incorrectKeys} />);
    expect(container.querySelector('.picture__scaffold')).toBeInTheDocument();
  });
});
