import React, { FC } from 'react';

interface PictureProps {
  incorrectKeys: string[];
}

const Picture: FC<PictureProps> = ({ incorrectKeys }) => (
  <svg
    className="picture"
    width="338"
    height="409"
    viewBox="0 0 338 409"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g className="picture__scaffold">
      <path
        d="M29 379H191.635"
        stroke="#4A2502"
        strokeWidth="20"
        strokeLinejoin="round"
      />
      <line
        x1="77"
        y1="387.42"
        x2="77"
        y2="23"
        stroke="#4A2502"
        strokeWidth="30"
      />
      <line
        y1="-7.5"
        x2="90"
        y2="-7.5"
        transform="matrix(0.650733 -0.759307 0.785122 0.619342 91.5 108.338)"
        stroke="#4A2502"
        strokeWidth="15"
      />
      <path d="M287.75 33L84.5 33" stroke="#4A2502" strokeWidth="20" />
      <line
        x1="268"
        y1="108"
        x2="268"
        y2="23"
        stroke="#9E4E04"
        strokeWidth="3"
      />
    </g>
    <g className="picture__hangman">
      {incorrectKeys.length > 0 && (
        <path
          d="M286 125.5C286 136.691 277.352 145.5 267 145.5C256.648 145.5 248 136.691 248 125.5C248 114.309 256.648 105.5 267 105.5C277.352 105.5 286 114.309 286 125.5Z"
          stroke="#464646"
          strokeWidth="7"
        />
      )}

      {incorrectKeys.length > 1 && (
        <path d="M267 217L267 144.5" stroke="#464646" strokeWidth="7" />
      )}

      {incorrectKeys.length > 2 && (
        <line
          x1="266.965"
          y1="158.585"
          x2="240.085"
          y2="222.319"
          stroke="#464646"
          strokeWidth="7"
          strokeLinecap="round"
        />
      )}

      {incorrectKeys.length > 3 && (
        <line
          x1="267.065"
          y1="158.912"
          x2="293.282"
          y2="222.922"
          stroke="#464646"
          strokeWidth="7"
          strokeLinecap="round"
        />
      )}

      {incorrectKeys.length > 4 && (
        <line
          x1="266.388"
          y1="216.464"
          x2="240.829"
          y2="304.855"
          stroke="#464646"
          strokeWidth="7"
          strokeLinecap="round"
        />
      )}

      {incorrectKeys.length > 5 && (
        <>
          <line
            x1="291.213"
            y1="305.293"
            x2="267.399"
            y2="216.416"
            stroke="#464646"
            strokeWidth="7"
            strokeLinecap="round"
          />
          <g className="hangman-eyes">
            <line
              y1="-1.5"
              x2="8.09751"
              y2="-1.5"
              transform="matrix(0.702858 -0.71133 0.719502 0.694491 259.5 125.76)"
              stroke="#464646"
              strokeWidth="3"
            />
            <line
              y1="-1.5"
              x2="8.09863"
              y2="-1.5"
              transform="matrix(0.711231 0.702959 -0.711231 0.702958 257.5 120)"
              stroke="#464646"
              strokeWidth="3"
            />
            <line
              y1="-1.5"
              x2="8.09863"
              y2="-1.5"
              transform="matrix(0.711231 -0.702958 0.711231 0.702958 271.5 125.693)"
              stroke="#464646"
              strokeWidth="3"
            />
            <line
              y1="-1.5"
              x2="8.1016"
              y2="-1.5"
              transform="matrix(0.732898 0.680338 -0.688849 0.724905 269.3 120)"
              stroke="#464646"
              strokeWidth="3"
            />
          </g>
        </>
      )}
    </g>
  </svg>
);

export default Picture;
