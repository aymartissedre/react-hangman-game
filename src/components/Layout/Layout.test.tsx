import { render } from '@testing-library/react';
import Layout from './Layout';

it('should render the Layout with correct children', () => {
  const { getByText } = render(
    <Layout>
      <div>Hello</div>
    </Layout>,
  );
  const divElement = getByText('Hello');
  expect(divElement).toBeInTheDocument();
});
