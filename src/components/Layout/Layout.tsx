import React, { FC } from 'react';
import './layout.scss';

interface LayoutProps {
  /* eslint-disable */
  children?: React.ReactNode;
}

const Layout: FC<LayoutProps> = ({ children }): JSX.Element => (
  <main className="main">{children}</main>
);

export default Layout;
