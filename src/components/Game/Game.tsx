import React, { FC, useEffect, useState } from 'react';
import Word from '../Word/Word';
import Keyboard from '../Keyboard/Keyboard';
import { WordType } from '../../api/models';
import Picture from '../Picture/Picture';
import './_game.scss';

interface GameProps {
  words: WordType[];
}

const Game: FC<GameProps> = ({ words }) => {
  // Initial list of words from props
  const [wordList, setWordList] = useState<WordType[]>(words);
  // Random picked word from wordList
  const [fetchedWord, setFetchedWord] = useState<WordType>(
    wordList[Math.floor(Math.random() * wordList.length)],
  );
  // List of incorrect keys input by the user
  const [incorrectKeys, setIncorrectKeys] = useState<string[]>([]);
  // List of correct keys input by the user
  const [correctKeys, setCorrectKeys] = useState<string[]>([]);
  const numberOfLives = 6;
  const [showWord, setShowWord] = useState<boolean>(false);
  const [score, setScore] = useState<number>(0);
  const [consecutiveWins, setConsecutiveWins] = useState<number>(0);

  // Lowercase version of fetchedWord to allow case-insensitive input by user
  const activeWord: WordType = {
    ...fetchedWord,
    word: fetchedWord.word.toLowerCase(),
  };

  const isGameWon = activeWord.word
    .replace(/ /g, '') // removing spaces because they are not to be filled by the user
    .split('')
    // Split the active word into letters : are they all present in the correct letters array ? If yes, game is won.
    .every((letter) => correctKeys.includes(letter));

  const isGameLost = numberOfLives <= incorrectKeys.length;

  // Updates score and consecutive wins after each game.
  useEffect(() => {
    if (isGameWon) {
      setConsecutiveWins((prevWins) => prevWins + 1);
      setScore((prevScore) => prevScore + 1000 - incorrectKeys.length * 100); // +1000 for finding the word. -100 for each incorrect key.
      // Removes the current word from the list to make sure we have to find another one in the next game.
      setWordList((prevWordList) => {
        if (prevWordList.length <= 1) {
          return words;
        }
        return [...prevWordList.filter((w) => w.word !== fetchedWord.word)];
      });
    }
    if (isGameLost) {
      setConsecutiveWins(0);
    }
  }, [isGameWon, isGameLost, incorrectKeys.length, fetchedWord.word, words]);

  const searchButton = (
    <a
      rel="noreferrer"
      target="_blank"
      href={`https://www.google.com/search?q=what+is+${fetchedWord.word}?`}
      className="button button--secondary"
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
        viewBox="0 0 48 48"
      >
        <defs>
          <path
            id="a"
            d="M44.5 20H24v8.5h11.8C34.7 33.9 30.1 37 24 37c-7.2 0-13-5.8-13-13s5.8-13 13-13c3.1 0 5.9 1.1 8.1 2.9l6.4-6.4C34.6 4.1 29.6 2 24 2 11.8 2 2 11.8 2 24s9.8 22 22 22c11 0 21-8 21-22 0-1.3-.2-2.7-.5-4z"
          />
        </defs>
        <clipPath id="b">
          <use xlinkHref="#a" overflow="visible" />
        </clipPath>
        <path clipPath="url(#b)" fill="#FBBC05" d="M0 37V11l17 13z" />
        <path
          clipPath="url(#b)"
          fill="#EA4335"
          d="M0 11l17 13 7-6.1L48 14V0H0z"
        />
        <path
          clipPath="url(#b)"
          fill="#34A853"
          d="M0 37l30-23 7.9 1L48 0v48H0z"
        />
        <path clipPath="url(#b)" fill="#4285F4" d="M48 48L17 24l-4-3 35-10z" />
      </svg>
      Search &quot;{fetchedWord.word}&quot; on Google
    </a>
  );

  const handleOnKeyPress = (button: string) => {
    // If active word has selected letter
    if (activeWord.word.includes(button)) {
      // Add letter in state only if it's not already in it
      if (!correctKeys.includes(button)) {
        setCorrectKeys((prevKeys) => [...prevKeys, button]);
      }
    } else if (!incorrectKeys.includes(button)) {
      setIncorrectKeys((prevKeys) => [...prevKeys, button]);
    }
  };

  const resetGame = () => {
    setFetchedWord(wordList[Math.floor(Math.random() * wordList.length)]);
    setCorrectKeys([]);
    setIncorrectKeys([]);
    setShowWord(false);
  };

  const renderGameWon = () => (
    <div className="game-won">
      <span>Well done! You guessed it right.</span>
      {fetchedWord.image && (
        <div className="game-won__image">
          <img
            className="word-image"
            src={fetchedWord.image}
            alt={fetchedWord.word}
          />
        </div>
      )}
      <div className="actions">
        <button type="button" className="button" onClick={resetGame}>
          Play again
        </button>
        {searchButton}
      </div>
    </div>
  );

  const renderGameLost = () => (
    <div className="game-lost">
      <p>
        <span className="bold">Oh no ! </span>
        Better luck next time... 😔
      </p>
      <p>What do you want to do ?</p>
      <div className="actions">
        <button type="button" className="button" onClick={resetGame}>
          Try again
        </button>
        {!showWord ? (
          <button
            type="button"
            className="button button--secondary"
            onClick={() => setShowWord(true)}
          >
            Show me the word !
          </button>
        ) : (
          <span>
            The word was&nbsp;
            <span className="special">{fetchedWord.word}</span>
          </span>
        )}
        {showWord && searchButton}
      </div>
    </div>
  );

  return (
    <div className="game">
      <div className="game__score">
        <p>Score : {score}</p>
        <p>Consecutive wins : {consecutiveWins}</p>
      </div>
      {!(isGameWon || isGameLost) && (
        <div className="game__word">
          <Word word={activeWord} correctKeys={correctKeys} />
        </div>
      )}
      <div className="game__picture">
        <Picture incorrectKeys={incorrectKeys} />
      </div>
      {!(isGameWon || isGameLost) && (
        <div className="game__keyboard">
          <Keyboard
            onKeyPress={handleOnKeyPress}
            pressedKeys={{
              correctKeys: correctKeys.join(' '),
              incorrectKeys: incorrectKeys.join(' '),
            }}
          />
        </div>
      )}

      {(isGameWon || isGameLost) && (
        <div className="game__over">
          {isGameWon && renderGameWon()}
          {isGameLost && renderGameLost()}
        </div>
      )}
    </div>
  );
};
export default Game;
